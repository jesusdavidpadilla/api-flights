# Api-Flights Change Log

## 1.0.0 | 26-04-2018 

### First versión Api-Flights

En esta primera versión la API REST dispone de los siguientes endpoints para brindar información de ciudades y vuelos:
- '/cities'
- '/city/:id'
- '/flights'
- '/flight/:id'
- '/find/:cityFrom/:cityTo/:date'
- '/find/:cityFrom/:cityTo/:dateFrom/:dateReturn'
- '/find/multiflights'