import { routesCity } from './cities/routes'
import { routesFlight } from './flight/routes'

export const allRoutes = [
    routesFlight,
    routesCity
]