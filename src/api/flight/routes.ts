import { ControllerFlights } from './controller'

export const routesFlight = {
  flights: {
    verb: 'get',
    mountPoint: '/flights',
    handler: ControllerFlights.flights
  },
  flight: {
    verb: 'get',
    mountPoint: '/flight/:id',
    handler: ControllerFlights.flight
  },
  getFlights: {
    verb: 'get',
    mountPoint: '/find/:cityFrom/:cityTo/:date',
    handler: ControllerFlights.getFlights
  },
  getRoundFlights: {
    verb: 'get',
    mountPoint: '/find/:cityFrom/:cityTo/:dateFrom/:dateReturn',
    handler: ControllerFlights.getRoundFlights
  },
  getMultiFlights: {
    verb: 'post',
    mountPoint: '/find/multiflights',
    handler: ControllerFlights.getMultiFlights
  }
}