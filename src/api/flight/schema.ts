import { Document, Model, model, Schema} from 'mongoose'
import { IFlights } from '../../interfaces'

export interface FlightModel extends IFlights, Document {}

export const FlightSchema: Schema = new Schema({
    destination: Object,
    image: String,
    segments: String
})

export const Flights: Model<FlightModel> = model<FlightModel>('Flights', FlightSchema)