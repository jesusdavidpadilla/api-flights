import express from 'express'
import request from 'supertest'
import { Core } from '../../core'
import ConnectDatabase from '../../database'
import { ControllerFlights } from './controller'
import { routesFlight } from './routes'

const app = express()
const flightRoutes = express.Router()

flightRoutes.get('/flights', ControllerFlights.flights)
.get('/flight/:id', ControllerFlights.flight)
.get('/find/:cityFrom/:cityTo/:date', ControllerFlights.getFlights)
.get('/find/:cityFrom/:cityTo/:dateFrom/:dateReturn', ControllerFlights.getRoundFlights)

app.use(flightRoutes)

const url = 'mongodb://almundo:123456@ds125469.mlab.com:25469/almundo-flights'
ConnectDatabase.conection(url)

describe('Loading server express', () => {
    it('Check connection', () => {
        return request(app).get('/flights')
            .expect(200).then((res) => expect(res.status).toEqual(200))
    })
})

describe('Check routes services', () => {
    it('Check GET All Flights', () => {
        return request(app).get('/flights')
            .expect(200).then((res) => {
                expect(Array.isArray(res.body.flights)).toBe(true)
            })
    })
    it('Check GET keys specific flight', () => {
        return request(app).get('/flight/5ae12f02734d1d48c4cbd000')
            .expect(200).then((res) => {
                const keysExpect = Object.keys(res.body.flight)
                const KeysEquals = ['_id', 'destination', 'origin', 'price', 'airline', 'detail', 'segments']
                const different = keysExpect.every((element) =>  KeysEquals.includes(element))
                expect(different).toBe(true)
        })
    })
    it('Check oneWay flight', () => {
        return request(app).get('/find/MIA/CLI/2018-05-05')
            .expect(200).then((res) => {
                expect(Array.isArray(res.body.flight)).toBe(true)
        })
    })
    it('Check roundWay flight', () => {
        return request(app).get('/find/CLI/MDE/2018-05-10/2018-05-10')
            .expect(200).then((res) => {
                const keysExpect = Object.keys(res.body.flight)
                const KeysEquals = ['group', 'from', 'return']
                const different = keysExpect.every((element) =>  KeysEquals.includes(element))
                expect(different).toBe(true)
        })
    })
})
