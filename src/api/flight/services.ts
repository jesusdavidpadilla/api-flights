import { Flights } from './schema'

export const Services = {

    getALLFlights: async () => {
        try {
            const flights = await Flights.find()
            return flights
        } catch (error) {
            return error
        }
    },
    getFlight: async (id: string) => {
        try {
            const flight = await Flights.findById(id)
            return flight
        } catch (error) {
            return error
        }
    },
    getFlights: async (cityFrom: string, cityTo: string, date: string) => {
        try {
            const flights = await Flights.find({
                'origin.iata': cityFrom,
                'origin.date': date,
                'destination.iata': cityTo
            })
            return flights
        } catch (error) {
            return error
        }
    },
    getRoundFlights: async (cityFrom: string, cityTo: string, dateFrom: string, dateReturn: string) => {
        try {
            const flightsFrom = await Services.getFlights(cityFrom, cityTo, dateFrom)
            const flightsReturn = await Services.getFlights(cityTo, cityFrom, dateReturn)
            const minFlights = Math.min(flightsFrom.length, flightsReturn.length)
            const grouped = []
            const flgFrom = []
            const flgReturn = []

            for (let i = 0; i < minFlights; i++) {
                grouped.push({ flightFrom: flightsFrom[i], flightTo: flightsReturn[i] })
            }

            flgFrom.push(...flightsFrom.slice(minFlights))
            flgReturn.push(...flightsReturn.slice(minFlights))

            return { group: grouped, from: flgFrom, return: flgReturn }
        } catch (error) {
            return error
        }
    },
    getMultiFlights: async (params: any) => {
        try {
            const multiFlights: any = {}
            for (let i = 0; i < params.length; i++) {
                multiFlights[`Flight${i + 1}`] =
                await Services.getFlights(params[i].cityFrom, params[i].cityTo, params[i].date)
            }
            const { Flight1, Flight2, Flight3 } = multiFlights
            const res = (Flight1.length > 0 && Flight2.length > 0 && Flight3.length > 0) ?
                Flight1 : { Flight1: 'Vuelos no encontrados' }
            return res
        } catch (error) {
            return error
        }
    }
}