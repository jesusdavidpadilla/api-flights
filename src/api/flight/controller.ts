import { NextFunction, Request, Response } from 'express'
import { registerLog } from '../../core/logger'
import { Services } from './services'

export const ControllerFlights = {
    flights: async (req: Request, res: Response, next: NextFunction) => {
        registerLog('info', 'Request getAllFlights')
        try {
            const flights = await Services.getALLFlights()
            res.send({ flights })
        } catch (err) {
            registerLog('error', `Error request ${err}`)
            res.send({ err })
        }
    },
    flight: async (req: Request, res: Response, next: NextFunction) => {
        registerLog('info', 'Request specific flight')
        try {
            const flight = await Services.getFlight(req.params.id)
            res.send({ flight })
        } catch (err) {
            registerLog('error', `Error request ${err}`)
            res.send({ err })
        }
    },
    getFlights: async (req: Request, res: Response, next: NextFunction) => {
        const {cityFrom, cityTo, date} = req.params
        registerLog('info', 'Request getFlights onway')
        try {
            const flight = await Services.getFlights(cityFrom, cityTo, date)
            res.send({ flight })
        } catch (err) {
            registerLog('error', `Error request ${err}`)
            res.send({ err })
        }
    },
    getRoundFlights: async (req: Request, res: Response, next: NextFunction) => {
        const {cityFrom, cityTo, dateFrom, dateReturn} = req.params
        registerLog('info', 'Request getFlights roundway')
        try {
            const flight = await Services.getRoundFlights(cityFrom, cityTo, dateFrom, dateReturn)
            res.send({ flight })
        } catch (err) {
            registerLog('error', `Error request ${err}`)
            res.send({ err })
        }
    },
    getMultiFlights: async (req: Request, res: Response, next: NextFunction) => {
        registerLog('info', 'Request getFlights multiflights')
        try {
            const flight = await Services.getMultiFlights(req.body)
            res.send({ flight })
        } catch (err) {
            registerLog('error', `Error request ${err}`)
            res.send({ err })
        }
    }
}