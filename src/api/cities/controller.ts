import { NextFunction, Request, Response } from 'express'
import { registerLog } from '../../core/logger'
import { Services } from './services'

export const ControllerCities = {
    cities: async (req: Request, res: Response, next: NextFunction) => {
        registerLog('info', 'Request getCitites')
        try {
            const cities = await Services.getCities()
            res.send({ cities })
        } catch (err) {
            registerLog('error', `Error request ${err}`)
            res.send({ err })
        }
    },
    city: async (req: Request, res: Response, next: NextFunction) => {
        registerLog('info', 'Request city')
        try {
            const city = await Services.getCity(req.params.id)
            res.send({ city })
        } catch (err) {
            registerLog('error', `Error request ${err}`)
            res.send({ err })
        }
    }
}