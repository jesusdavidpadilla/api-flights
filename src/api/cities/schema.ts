import { Document, Model, model, Schema} from 'mongoose'
import { ICities } from '../../interfaces'

export interface CityModel extends ICities, Document {}

export const CitySchema: Schema = new Schema({
    nameLarge: String,
    aeroName: String
})

export const Cities: Model<CityModel> = model<CityModel>('Cities', CitySchema)