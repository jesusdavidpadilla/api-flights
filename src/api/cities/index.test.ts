import express from 'express'
import request from 'supertest'
import { Core } from '../../core'
import ConnectDatabase from '../../database'
import { ControllerCities } from './controller'
import { routesCity } from './routes'

const app = express()
const flightRoutes = express.Router()

flightRoutes.get('/cities', ControllerCities.cities)
.get('/city/:id', ControllerCities.city)

app.use(flightRoutes)

const url = 'mongodb://almundo:123456@ds125469.mlab.com:25469/almundo-flights'
ConnectDatabase.conection(url)

describe('Loading server express', () => {
    it('Check connection', () => {
        return request(app).get('/cities')
            .expect(200).then((res) => expect(res.status).toEqual(200))
    })
})

describe('Check routes services', () => {
    it('Check GET All Cities', () => {
        return request(app).get('/cities')
            .expect(200).then((res) => {
                expect(Array.isArray(res.body.cities)).toBe(true)
            })
    })
    it('Check GET keys specific city', () => {
        return request(app).get('/city/5ae0863c734d1d48c4cb3492')
            .expect(200).then((res) => {
                const keysExpect = Object.keys(res.body.city)
                const KeysEquals = ['_id', 'nameLarge', 'aeroName']
                const different = keysExpect.every((element) =>  KeysEquals.includes(element))
                expect(different).toBe(true)
        })
    })
})
