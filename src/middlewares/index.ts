import bodyParser from 'body-parser'
import { createNamespace } from 'continuation-local-storage'
import cors from 'cors'
import { NextFunction, Request, Response } from 'express'
import uuid from 'uuid'
import { allRoutes } from '../api/routes'

const myRequest = createNamespace('request')

export const routesServer = allRoutes

export const ThirdPartyMiddlewares = {
  cors: {
    handler: cors()
  },
  urlEncoded: {
    handler: bodyParser.urlencoded({ extended: false })
  },
  jsonParser: {
    handler: bodyParser.json()
  },
  logger: {
    handler: (req: Request, res: Response, next: NextFunction) => {
      myRequest.run(() => {
        myRequest.set('reqId', uuid.v1())
        next()
      })
    }
  }
}