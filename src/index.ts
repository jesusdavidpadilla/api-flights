process.env.NODE_CONFIG_DIR = `${__dirname}/env`
import config from 'config'
import express from 'express'

import { Core } from './core/'
import { ThirdPartyMiddlewares } from './middlewares'
import { routesServer } from './middlewares'

export default class Server extends Core {
    constructor() {
      super()
      this.mountThirdPartyMiddlewares(ThirdPartyMiddlewares)
      this.mountRoutes(routesServer)
    }

    public static bootstrap(): Server {
      return new Server()
    }
  }

Server.bootstrap().startServer()
