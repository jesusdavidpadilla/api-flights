import { getNamespace } from 'continuation-local-storage'
import * as winstonLogger from 'winston'

winstonLogger.configure({
  level: 'debug',
  transports: [
    new winstonLogger.transports.File({
      level: 'debug',
      filename: './src/core/logger/logs/all-logs.log',
      handleExceptions: true,
      json: true,
      maxsize: 5242880,
      maxFiles: 5,
      colorize: false,
      timestamp: true
    }),
    new winstonLogger.transports.Console({
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true,
      timestamp: true
    })
  ],
  exitOnError: false,
  stream: {
    write(message: any, encoding: any) {
      winstonLogger.info(message)
    }
  }
})

const formatMessage = (message: string) => {
  const request = getNamespace('request')
  message = request && request.get('reqId') ? `${message} ' reqId: ' ${request.get('reqId')}` : message
  return message
}

// Tipos de logs disponibles 'error', 'warn', 'verbose', 'info', 'debug', 'silly'
export const registerLog = (type: string, menssaje: string) => {
  winstonLogger[type](formatMessage(menssaje))
}