import config from 'config'
import express, { Request, Response } from 'express'
import ConnectDatabase from '../database'
import { IData } from '../interfaces'
import { registerLog } from './logger'

class ExpressClass implements IData {
    public middleware: express.Application
    constructor() {
        this.middleware = express()
    }
}

export class Core {
    public middleware: any
    public conf: string

    constructor() {
        this.middleware = new ExpressClass().middleware
        this.conf = config.get('server')
    }

    public use(...args: any[]) {
        this.middleware.use(...args)
        return this
    }

    public connectBD() {
        const { initUrl, lastUrl, user, pwd } = this.conf as any
        const signatureUrl = `${initUrl}${user}:${pwd}${lastUrl}`
        ConnectDatabase.conection(signatureUrl)
    }

    public mountRoutes(routes: any) {
        routes.map(
            (routeGroup: any) => {
                Object.keys(routeGroup).forEach((key) => {
                    this.middleware[routeGroup[key].verb](routeGroup[key].mountPoint, routeGroup[key].handler)
                })
            }
        )
    }

    public mountThirdPartyMiddlewares(middleware: any) {
        Object.keys(middleware).forEach((key) => {
            this.use(middleware[key].handler)
        })
    }

    public mountMiddlewares(middleware: any) {
        Object.keys(middleware).forEach((key) => {
            this.use(middleware[key].mountPoint, middleware[key].handler)
        })
    }

    public listen(handle: any, listeningListener: Function | undefined) {
        this.middleware.listen(handle, listeningListener)
    }

    public startServer() {
        this.connectBD()
        ConnectDatabase.LoggerBD()
        this.listen((this.conf as any).port, () => {
            registerLog('info', 'Start listen server')
         })
    }

}
