export interface IData {
    [key: string]: any
}

export interface IDestination {
    iata: string
    name: string
}

export interface IFlights {
    destination: IDestination
    image: string
    segments: string
}

export interface ICities {
    nameLarge: string
    aeroName: string
  }