import config from 'config'
import mongoose from 'mongoose'
import { registerLog } from '../core/logger'
mongoose.Promise = global.Promise

export default class ConnectDatabase {
    public static conection(url: string) {
        const optionsMongo = {
            keepAlive: 120
        }
        try {
            mongoose.connect(url, optionsMongo)
        } catch (err) {
            mongoose.createConnection(url, optionsMongo)
        }
        mongoose.connection
            .once('open', () => registerLog('info', 'Conexion a MongoDB realizada correctamente'))
            .on('error', (err) => {
                registerLog('error', err)
                throw err
            })
    }

    public static LoggerBD() {
        mongoose.set('debug', (...args: any []) => {
            const set = {...args}
        })
    }
}
