# API REST - FLIGHTS

API desarrollada mediante en TypeScript, express y MongoDB utilizada como fuente de información para la aplicación desarrollada en React Native correspondeinte a la prueba final del Semillero Almundo Medellín - Colombia.

# Getting Started

Para utilización de la API en su maquina local deberá cumplir con algunos requisitos y ejecutar las siguientes instrucciones

# Requirements

- Poseer el entorno de desarrollo Node
- Clonar el repositorio

# Installation

- Desde la terminar de su maquina, navegar entre carpetas hasta llegar a la del repositorio clonado.
- Ejecutar el comando:

> npm install

El comando anterior instalará todas las dependencias del proyecto.

# Running Dev

Para iniciar la API en modo de desarrollo ejecute el comando:

> npm run dev

# Run Test

Para correr los test de los endpoints mediante Jest ejecutar el comando:

> npm run test


# Demo

Actualmente puedes consultar la API REST alojada en now mediante la URL https://flights-xpzvgavtrp.now.sh

# EndPoints

## Get All Cities

Route: '/cities'
Method: GET
Example Sucess Response: 
{
    cities: [{
        _id: "5ae085bb734d1d48c4cb3416",
        nameLarge: "Santiago de Cali",
        aeroName: "CLI"
        },{
        _id: "5ae085ee734d1d48c4cb343b",
        nameLarge: "Buenos Aires",
        aeroName: "EZE"
        },...]
}

## Get specific city

Route: '/city/:id'
Method: GET
Data Example: '/city/5ae085bb734d1d48c4cb3416'
Example Sucess Response: 
{
    city: {
            _id: "5ae085bb734d1d48c4cb3416",
            nameLarge: "Santiago de Cali",
            aeroName: "CLI"
        }
}

## Get All Flights

Route: '/flights'

Method: GET

Example Sucess Response: 

{

    flights: [{
        _id: "5add1e97734d1d0b3c2dfa64",
        destination: {
            date: "2018-05-05",
            hour: "8:00 AM",
            iata: "CLI",
            name: "Santiago de Cali",
            img: "https://image.ibb.co/chLoKH/cali.jpg"
        },
        origin: {
            date: "2018-05-05",
            hour: "12:00 AM",
            iata: "EZE",
            name: "Buenos Aires",
            img: "https://image.ibb.co/m43VYc/buenosaires.png"
            },
        price: {
                value: 2800000,
                currency: "COP"
            },
        airline: {
                img: "https://image.ibb.co/cxpAjH/avianca.png",
                name: "Avianca"
            },
        detail: [{
                icon: "work",
                tittle: "Equipaje",
                description: "Solo equipaje de mano"
            },{
                icon: "flight",
                tittle: "Avión",
                description: "Airbus 320"
            },{
                icon: "class",
                tittle: "Clase",
                description: "Económica"
            },{
                icon: "flight",
                tittle: "Tiempo de vuelo",
                description: "0h:40m"
            }],
        segments: []
    },
    ...]

}

## Get specific flight

Route: '/flight/:id'

Method: GET

## Get flight OneWay

Route: '/find/:cityFrom/:cityTo/:date'

Method: GET

Data Example: '/find/MDE/CLI/2018-05-10'

Example Sucess Response: 

{

    flight: [{
            _id: "5ae131a1734d1d48c4cbd097",
            destination: {
            date: "2018-05-10",
            hour: "7:00 PM",
            iata: "CLI",
            name: "Santiago de Cali",
            img: "https://image.ibb.co/chLoKH/cali.jpg"
        },
        origin: {
            date: "2018-05-10",
            hour: "5:00 PM",
            iata: "MDE",
            name: "Medellin",
            img: "https://preview.ibb.co/eRvcDc/medellin.png"
        },
        price: {
            value: 600000,
            currency: "COP"
        },
        airline: {
            img: "https://image.ibb.co/jwo34H/latam.jpg",
            name: "LATAM"
        },
        detail: [{
            icon: "work",
            tittle: "Equipaje",
            description: "Solo equipaje de mano"
        },{
            icon: "flight",
            tittle: "Avión",
            description: "Airbus 320"
        },{
            icon: "class",
            tittle: "Clase",
            description: "Económica"
        },{
            icon: "flight",
            tittle: "Tiempo de vuelo",
            description: "1h:00m"
        }],
        segments: []
    },{
        _id: "5ae171cb734d1d48c4cbecf5",
        destination: {
            date: "2018-05-10",
            hour: "6:00 PM",
            iata: "CLI",
            name: "Santiago de Cali",
            img: "https://image.ibb.co/chLoKH/cali.jpg"
        },
        origin: {
            date: "2018-05-10",
            hour: "3:00 PM",
            iata: "MDE",
            name: "Medellin",
            img: "https://preview.ibb.co/eRvcDc/medellin.png"
        },
        price: {
            value: 54000,
            currency: "COP"
        },
        airline: {
            img: "https://image.ibb.co/jwo34H/latam.jpg",
            name: "LATAM"
        },
        detail: [{
            icon: "work",
            tittle: "Equipaje",
            description: "Solo equipaje de mano"
        },{
            icon: "flight",
            tittle: "Avión",
            description: "Airbus 320"
        },{
            icon: "class",
            tittle: "Clase",
            description: "Económica"
        },{
            icon: "flight",
            tittle: "Tiempo de vuelo",
            description: "1h:00m"
        }],
        segments: []
        }
    ]

}

## Get flight RoundWay

Route: '/find/:cityFrom/:cityTo/:dateFrom/:dateReturn'

Method: GET

Data Example: '/find/MDE/CLI/2018-05-10/2018-05-10'


## Get flight RoundWay

Route: '/find/multiflights'

Method: POST


# Versión

1.0.0


# Autores

Jesús David Padilla